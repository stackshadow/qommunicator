package message

import (
	"encoding/json"
	"fmt"
)

// JSONString will convert an message to an json string
func (curMessage *Message) JSONString() (string, error) {

	b, err := json.Marshal(curMessage)
	if err != nil {
		fmt.Println("error:", err)
	}

	return string(b), nil
}
