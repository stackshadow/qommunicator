package message

import "github.com/rs/zerolog/log"

func (suite *TestSuite) TestMessage() {
	jsonData := `
	{
		"r": "requestID",
		"s": "2006-01-02T15:04:05Z07:00",
		"t": "/house/room/lamp",
		"v": "on"
	}
	`
	var err error
	var message Message
	message, err = NewJSON(jsonData)
	suite.Assert().Nil(err)

	log.Debug().Object("message", message).Send()

	suite.Assert().Equal("requestID", message.ReqID)
	suite.Assert().Equal("/house/room/lamp", message.Topic)
	suite.Assert().Equal("on", message.Payload)

}
