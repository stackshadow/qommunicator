package message

// SendAsJSON;
//
// Can be used to send this message as json to evualuate it on the remove side
//
// For example, the timestamp can be used, to store the value with the timestamp, not the arrival-timestamp
// Or the remote can react on the TopicReply-Field

// Message represent a single message from/to SSE
type Message struct {

	// id is the ID of the message
	id string

	// SendAsJSON will send this message as json to the Topic
	SendAsJSON bool `json:"o,omitempty"`

	// ReqID [OPTIONAL] represents the current request
	ReqID string `json:"r,omitempty"`

	// The topic
	Timestamp JSONTime `json:"s,omitempty"`

	// The topic
	Topic string `json:"t,omitempty"`

	// TopicReply [OPTIONAL] - the request should reply with this topic
	TopicReply string `json:"tr,omitempty"`

	// payload
	Payload string `json:"v"`
}
