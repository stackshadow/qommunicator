package message

func (suite *TestSuite) TestJsonTime() {

	var err error

	jsonData := `
	{
		"s": "2021-12-03T18:20:42+01:00"
	}
	`
	var message Message
	message, err = NewJSON(jsonData)
	suite.Assert().Nil(err)

	var timeData []byte
	timeData, err = message.Timestamp.MarshalJSON()
	suite.Assert().Nil(err)

	suite.Assert().Equal(string(timeData), "\"2021-12-03T18:20:42+01:00\"")
}
