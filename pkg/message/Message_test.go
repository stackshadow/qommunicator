package message

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type TestSuite struct {
	suite.Suite
}

func TestDoIt(t *testing.T) {
	suite.Run(t, new(TestSuite))
}
