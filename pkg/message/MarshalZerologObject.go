package message

import "github.com/rs/zerolog"

// MarshalZerologObject implementation to use it with Object()
func (curMessage Message) MarshalZerologObject(e *zerolog.Event) {

	payloadLen := len(curMessage.Payload)
	if payloadLen > 25 {
		payloadLen = 25
	}

	e.
		Str("msgID", curMessage.id).
		Str("msgReqID", curMessage.ReqID).
		Str("msgTopic", curMessage.Topic).
		Str("msgTopicReply", curMessage.TopicReply).
		Str("msgPayload", curMessage.Payload[0:payloadLen])

}
