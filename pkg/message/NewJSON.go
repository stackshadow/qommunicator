package message

import (
	"encoding/json"
)

// NewJSON will convert an json-string to an message
func NewJSON(jsonString string) (Message, error) {

	var newMessage Message

	err := json.Unmarshal([]byte(jsonString), &newMessage)
	if err != nil {
		return newMessage, err
	}

	return newMessage, nil
}
