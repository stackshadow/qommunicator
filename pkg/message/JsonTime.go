package message

import (
	"fmt"
	"strings"
	"time"
)

// JSONTime represent an timestamp
type JSONTime time.Time

// MarshalJSON will mashall an timestamp
func (t JSONTime) MarshalJSON() ([]byte, error) {
	//do your serializing here
	stamp := fmt.Sprintf("\"%s\"", time.Time(t).Format(time.RFC3339))
	return []byte(stamp), nil
}

// UnmarshalJSON unmashall the timestapm
func (t *JSONTime) UnmarshalJSON(data []byte) (err error) {

	timeString := strings.Replace(string(data), "\"", "", -1)

	// covert it
	var tm time.Time
	tm, err = time.Parse(time.RFC3339, timeString)

	// store it
	if err == nil {
		*t = JSONTime(tm)
	}

	return nil
}
