package session

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run

func (suite *TestSuite) TestAuthenticated() {

	session := New("integrationtest")

	// set it explicit to false
	session.AuthenticatedSet(false)
	suite.Assert().False(session.IsAuthenticated())

	// try to set it to true ( should be stay false)
	session.AuthenticatedSet(true)
	suite.Assert().False(session.IsAuthenticated())
}
