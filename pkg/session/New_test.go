package session

func (suite *TestSuite) TestNew() {

	curSession := New("")
	suite.Assert().Empty(curSession.ID())

	curSession.Reset("")
	suite.Assert().NotEmpty(curSession.ID())

}
