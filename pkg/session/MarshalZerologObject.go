package session

import "github.com/rs/zerolog"

// MarshalZerologObject implementation to use it with Object()
func (s Session) MarshalZerologObject(event *zerolog.Event) {

	event.
		Str("id", s.id).
		Str("updated at", s.updatedAt.String()).
		Bool("authenticated", s.authenthicated)

}
