package session

func (suite *TestSuite) TestInternal() {

	newSession := New("integrationtest")

	// internal
	suite.Assert().False(newSession.IsInternal())
	newSession.InternalSet()
	suite.Assert().True(newSession.IsInternal())

}
