package session

import (
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
)

func New(remoteAddr string) (s Session) {
	return
}

// Init just init an existing session to initial state
//
// All data gets lost !
//
// remoteAddr can be empty
func (s *Session) Reset(remoteAddr string) {

	s.id = uuid.Must(uuid.NewRandom()).String()
	s.createdAt = time.Now()
	s.updatedAt = time.Now()
	s.username = ""
	s.remoteAddr = remoteAddr
	s.jwtKey = ""

	s.authenthicatedSet = false
	s.authenthicated = false

	s.toFrontend = nil

	if s.data == nil {
		s.data = make(map[string]interface{})
	}

	log.Debug().Object("session", s).Msg("Session resetted")
}
