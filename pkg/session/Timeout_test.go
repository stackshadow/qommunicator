package session

import "time"

func (suite *TestSuite) TestTimeout() {

	curSession := New("")
	curSession.Refresh()
	suite.Assert().False(curSession.IsTimedOut(time.Minute))

	time.Sleep(2 * time.Second)
	suite.Assert().True(curSession.IsTimedOut(time.Second))

}
