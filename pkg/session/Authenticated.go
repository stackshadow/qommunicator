package session

import "github.com/rs/zerolog/log"

func (s Session) IsAuthenticated() bool {
	return s.authenthicated
}

// AuthenticatedSet can set the authenticated-state to true/false ONLY ONCE
//
// you can set it to false every time
//
// but if its set to false, you can not set it to true again
func (s *Session) AuthenticatedSet(authenticated bool) {

	if authenticated {

		if !s.authenthicatedSet {
			s.authenthicatedSet = true
			s.authenthicated = authenticated
			log.Debug().Object("session", s).Msg("Session is now authenticated")
		} else {
			log.Warn().Object("session", s).Msg("Authentication was already set to true, you can not set it again to true")
		}

	} else {
		s.authenthicatedSet = true
		s.authenthicated = false
		log.Debug().Object("session", s).Msg("Session is no more authenticated")
	}

}
