package session

// ID will return the current sessionID
func (s Session) ID() string {
	return s.id
}

// IDSet will set the id of an session only if no ID was set before
func (s *Session) IDSet(newID string) {

	if s.id == "" {
		s.id = newID
	}

}
