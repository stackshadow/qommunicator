package session

import "time"

// SessionInfo represent information about a single session
type SessionInfo struct {
	ID         string    `json:"id,omitempty"`
	CreatedAt  time.Time `json:"timestamp,omitempty"` // when the session was created
	UpdatedAt  time.Time `json:"updated,omitempty"`   // when the session was created
	Username   string    `json:"username,omitempty"`  // the username or "" if anonym
	RemoteAddr string    `json:"ip,omitempty"`
}

// GetSessionInfos return all sessions for an given user
func (s Session) SessionInfo() (info SessionInfo) {

	info = SessionInfo{
		ID:         s.id,
		CreatedAt:  s.createdAt,
		UpdatedAt:  s.updatedAt,
		Username:   s.username,
		RemoteAddr: s.remoteAddr,
	}

	return
}
