package session

import "errors"

// DataSet will set some data in the session that is identified with name
func (s *Session) DataSet(name string, data interface{}) (err error) {
	if data == nil {
		err = errors.New("data is nil")
		return
	}

	// create data if needed
	if s.data == nil {
		s.data = make(map[string]interface{})
	}

	s.data[name] = data
	return
}

// Data will get data from the session that was set before with DataSet
func (s *Session) Data(name string) (data interface{}, err error) {

	var curdata interface{} = s.data[name]

	if curdata == nil {
		err = errors.New("data is nil")
		return nil, err
	}

	return curdata, nil
}
