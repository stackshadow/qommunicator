package session

import (
	"time"

	"gitlab.com/stackshadow/qommunicator/v2/pkg/message"
)

// Session represent a single session
type Session struct {
	id         string
	createdAt  time.Time // when the session was created
	updatedAt  time.Time // last use of this session
	username   string    // the username or "" if anonym
	remoteAddr string
	jwtKey     string // jwtKey of the signed JWT token

	authenthicatedSet bool // if one of our middlewares set the auth to true/false, nobody else can set it again
	authenthicated    bool // if auth was successful this is true

	// messages to send
	toFrontend chan *message.Message

	// data that can be store to an session by middlewares
	data map[string]interface{}
}
