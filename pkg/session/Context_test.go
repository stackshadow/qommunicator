package session

import "context"

func (suite *TestSuite) TestContext() {

	var err error

	ctx := context.TODO()
	newSession := New("integrationtest")
	newSession.IDSet("id")
	newSession.DataSet("somedata", 5)

	ctx = newSession.AddToContext(ctx)
	storedSession := FromContext(ctx)
	suite.Assert().Equal(newSession.remoteAddr, storedSession.remoteAddr)
	suite.Assert().Equal("id", newSession.ID())

	var interfaceData interface{}
	interfaceData, err = storedSession.Data("somedata")
	suite.Assert().Nil(err)
	suite.Assert().Equal(5, interfaceData)

}
