package session

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/message"
)

// Send will send an msg over server-sent-events ( if an client connected to it )
func (s *Session) Send(msg *message.Message) {

	if s.toFrontend == nil {
		log.Warn().
			Object("session", s).
			Object("message", msg).
			Msg("Frontend has no open server-send-events, can not send you message")
		return
	}

	s.toFrontend <- msg
}

// return the channel
func (s *Session) ToFrontend() <-chan *message.Message {

	if s.toFrontend == nil {
		s.toFrontend = make(chan *message.Message, 10)
	}

	return s.toFrontend

}

// close the channel to frontend
func (s *Session) CloseChannel() {
	// close channel
	if s.toFrontend != nil {
		close(s.toFrontend)
		s.toFrontend = nil
	}
}

// close the channel and remove all data
func (s *Session) Close() {
	// close channel
	s.CloseChannel()

	// remove the data
	for name := range s.data {
		delete(s.data, name)
	}
	s.data = nil

	log.Debug().Object("session", s).Msg("Session closed")
}
