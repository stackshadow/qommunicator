package session

import (
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"

	"github.com/lestrrat-go/jwx/jwa"
	"github.com/lestrrat-go/jwx/jwt"
)

// NewJWTToken
//
// - store the sessionID in JWT-Token
//
// - create new secret ( and overwrite an possible old secret !)
//
// - create and sign the jwt
func (s *Session) JWTNew() (signedToken []byte, err error) {

	// create jwt.Token
	jwtToken := jwt.New()
	jwtToken.Set(jwt.AudienceKey, `qommunicator`)
	jwtToken.Set(jwt.IssuedAtKey, time.Now().Unix())
	jwtToken.Set("sessionID", s.id)

	if s.jwtKey != "" {
		log.Warn().Object("session", s).Msg("Signing key already exist and will now be overwritten")
	}

	s.jwtKey = uuid.Must(uuid.NewRandom()).String()

	// sign token
	signedToken, err = jwt.Sign(jwtToken, jwa.HS384, []byte(s.jwtKey))

	return
}

// Verify check the JWT-token and return the sessionID
func (s *Session) JWTVerify(signedToken []byte) (valid bool) {
	_, err := jwt.Parse(signedToken, jwt.WithVerify(jwa.HS384, []byte(s.jwtKey)))
	if err == nil {
		valid = true
	}
	return
}
