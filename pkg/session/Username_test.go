package session

func (suite *TestSuite) TestUsername() {

	newSession := New("")

	newSession.UsernameSet("newuser")
	suite.Assert().Equal("newuser", newSession.Username())

}
