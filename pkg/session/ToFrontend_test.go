package session

import (
	"sync"

	"gitlab.com/stackshadow/qommunicator/v2/pkg/message"
)

func (suite *TestSuite) TestToFrontend() {

	var err error

	newSession := New("")

	var sended sync.Mutex
	sended.Lock()
	go func() {
		channel := newSession.ToFrontend()
		for {
			recvdMessage, open := <-channel

			if open {
				suite.Assert().Equal("test", recvdMessage.Topic)
				suite.Assert().Equal("data", recvdMessage.Payload)

				sended.Unlock()
			} else {
				break
			}

		}

	}()

	newMessage := message.Message{}
	newMessage.Topic = "test"
	newMessage.Payload = "data"
	newSession.Send(&newMessage)

	// we add some data that should be missing after close
	newSession.DataSet("test", 1)

	_, err = newSession.Data("notexist")
	suite.Assert().NotNil(err)

	_, err = newSession.Data("test")
	suite.Assert().Nil(err)

	newSession.Close()
	_, err = newSession.Data("test")
	suite.Assert().NotNil(err)

}
