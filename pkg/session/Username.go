package session

func (s *Session) UsernameSet(username string) {
	s.username = username
}

func (s Session) Username() (username string) {
	return s.username
}
