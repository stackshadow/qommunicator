package session

import "github.com/rs/zerolog/log"

func (s Session) IsInternal() bool {
	return s.id == "internal"
}

// InternalSet mark this session as internal session
//
// This is for internal stuff, like config changes on the hard drive or shutdown etc.
//
// This should NEVER set when it comes from external request
func (s *Session) InternalSet() {

	if s.id == "" {
		s.id = "internal"
		s.authenthicated = true
	} else {
		log.Warn().Object("session", s).Msg("This session can not marked as internal, it has already an ID")
	}

}
