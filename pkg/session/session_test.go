package session

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type TestSuite struct {
	suite.Suite
}

func TestSessionsMiddleware(t *testing.T) {
	suite.Run(t, new(TestSuite))
}
