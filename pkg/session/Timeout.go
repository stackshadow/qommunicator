package session

import (
	"time"

	"github.com/rs/zerolog/log"
)

func (s Session) IsTimedOut(timeout time.Duration) bool {
	return time.Now().After(s.updatedAt.Add(timeout))
}

// Refresh will refresh the timeout
func (s *Session) Refresh() {
	log.Debug().Object("session", s).Msg("Refresh session")
	s.updatedAt = time.Now()
}
