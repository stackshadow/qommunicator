package session

import (
	"context"

	"gitlab.com/stackshadow/qommunicator/v2/pkg/contextdata"
)

// AddToContext will add the session to the given context and return the new context
func (s *Session) AddToContext(ctx context.Context) (newCtx context.Context) {
	newCtx = context.WithValue(ctx, contextdata.SessionKey, s)
	return
}

// FromContext return the session from an context
// this works only if it was added before with AddToContext
func FromContext(ctx context.Context) (s *Session) {
	s, _ = ctx.Value(contextdata.SessionKey).(*Session)
	return
}
