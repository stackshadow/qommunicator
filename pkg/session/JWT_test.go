package session

func (suite *TestSuite) TestJWT() {
	var err error

	curSession := New("")

	var signedToken []byte
	signedToken, err = curSession.JWTNew()
	suite.Assert().Nil(err)

	valid := curSession.JWTVerify(signedToken)
	suite.Assert().Equal(true, valid)

}
