package middleware

import (
	"net/http"

	"gitlab.com/stackshadow/qommunicator/v2/pkg/session"
)

type V2 interface {
	Init()
	Name() string
	Handler(currentSession *session.Session, r *http.Request, w http.ResponseWriter) (changedSession *session.Session, err error)
}
