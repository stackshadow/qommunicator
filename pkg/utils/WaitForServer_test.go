package utils

import (
	"net/http"
	"testing"

	"github.com/go-chi/chi/v5"
)

func TestSessionsMiddleware(t *testing.T) {

	var err error

	// new router
	router := chi.NewRouter()
	router.Get("/up", func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(http.StatusOK) })
	go http.ListenAndServe("127.0.0.1:4646", router)

	err = WaitForServer("http://127.0.0.1:9988/up")
	if err == nil {
		t.FailNow()
	}

	err = WaitForServer("http://127.0.0.1:4646/up")
	if err != nil {
		t.FailNow()
	}

}
