package qerrors

// Errors raised by package x.
const (
	ErrCookieMissing  = constError("cookie missing")
	ErrCookieWrong    = constError("cookie not contains the correct values")
	ErrSessionUnknown = constError("session unknown")
)

type constError string

func (err constError) Error() string {
	return string(err)
}
