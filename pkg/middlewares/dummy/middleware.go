package dummy

import (
	"net/http"

	"github.com/rs/zerolog/log"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/session"
)

type Middleware struct {
}

func (m *Middleware) Init() {
}

func (m Middleware) Name() string {
	return "dummy"
}

func (m *Middleware) Handler(currentSession *session.Session, r *http.Request, w http.ResponseWriter) (changedSession *session.Session, err error) {

	/*
		var bodyBytes []byte

		bodyBytes, err = io.ReadAll(r.Body)
		if err != nil {
			return false, err
		}
	*/

	// we handle config ?
	if r.URL.Path == "login" {
		currentSession.AuthenticatedSet(true)
		currentSession.UsernameSet("dummy")
		changedSession = currentSession // this allow the next middleware
		err = nil
	}

	return
}

// HTTPMiddleware
func HTTPMiddleware(nextMiddleware http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {

		newSession := session.New("localhost")
		newSession.UsernameSet("integrationtest")
		newSession.AuthenticatedSet(true)

		r = r.WithContext(newSession.AddToContext(r.Context()))

		// call next middleware
		nextMiddleware.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

func LoggedIn(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		log.Debug().Msg("Logged in")
		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}
