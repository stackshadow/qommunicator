package stripprefix

import (
	"net/http"
	"strings"
)

// will remove the prefix from your request
func Middleware(prefix string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			r.URL.Path = strings.TrimPrefix(r.URL.Path, prefix)
			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}
