package acl

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"testing"

	"github.com/go-chi/chi/v5"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/suite"

	"gitlab.com/stackshadow/qommunicator/v2/pkg/middlewares/sessions"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/middlewares/stripprefix"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
)

type ACLTestSuite struct {
	suite.Suite
	host   string
	router *chi.Mux
}

type ACTTestConfig struct {
	Middlewares struct {
		Acl ConfigStruct `yaml:"acl" json:"acl"`
	} `yaml:"middlewares" json:"middlewares"`
}

func TestDynDBTestSuite(t *testing.T) {
	suite.Run(t, new(ACLTestSuite))
}

// setup suite
func (suite *ACLTestSuite) SetupSuite() {
	// logger
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	log.Logger = log.Logger.With().Caller().Logger()

	suite.host = "127.0.0.1:4501"

	// new router
	suite.router = chi.NewRouter()
	suite.router.Get("/up", func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(http.StatusOK) })

	suite.router.Route("/api/v1", func(r chi.Router) {

		r.Use(stripprefix.Middleware("/api/v1"))

		// endpoint + middleware to set the config
		r.Put("/config", func(w http.ResponseWriter, r *http.Request) {

			var err error
			defer utils.RespondOnPanic(w)

			// read from request
			var bodyData []byte
			bodyData, err = io.ReadAll(r.Body)
			utils.PanicOnErr(err)

			// parse
			var newConfig ACTTestConfig
			err = json.Unmarshal(bodyData, &newConfig)
			utils.PanicOnErr(err)

			// set config
			Set(newConfig.Middlewares.Acl)
		})

		r.Patch("/config", func(w http.ResponseWriter, r *http.Request) {

			var err error
			defer utils.RespondOnPanic(w)

			// read from request
			var bodyData []byte
			bodyData, err = io.ReadAll(r.Body)
			utils.PanicOnErr(err)

			// parse
			var newConfig ACTTestConfig
			err = json.Unmarshal(bodyData, &newConfig)
			utils.PanicOnErr(err)

			// patch
			Patch(newConfig.Middlewares.Acl)
		})

		// endpoint to create a new session ( typically an login success )
		r.Post("/login", func(w http.ResponseWriter, r *http.Request) {
			userNameBytes, err := ioutil.ReadAll(r.Body)
			if err != nil || len(userNameBytes) == 0 {
				userNameBytes = []byte{}
			}
			sessions.SetNewSessionCookie(w, r, string(userNameBytes))
		})

		// secured endpoints
		securedEndpoints := r.With(sessions.CheckMiddleware, CheckMiddleware)

		securedEndpoints.Get("/data/*", func(w http.ResponseWriter, r *http.Request) {})
		securedEndpoints.Get("/anon/*", func(w http.ResponseWriter, r *http.Request) {})

	})

	go http.ListenAndServe(suite.host, suite.router)
	utils.WaitForServer(fmt.Sprintf("http://%s/up", suite.host))

}

func (suite *ACLTestSuite) Test03_ForbiddenWithoutLogin() {
	// request response
	request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("http://%s/api/v1/data/set/bar", suite.host), nil)
	request.Method = "GET"

	// action
	//suite.router.Middlewares().Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})).ServeHTTP(response, request)
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()

	// we expect that our config was stored
	suite.Require().Equal(http.StatusUnauthorized, response.StatusCode)

}

func (suite *ACLTestSuite) Login(username string) (cookie *http.Cookie) {
	// request response
	request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("http://%s/api/v1/login", suite.host), nil)
	request.Method = "POST"
	request.Body = ioutil.NopCloser(bytes.NewReader([]byte(username)))

	// action
	//suite.router.Middlewares().Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})).ServeHTTP(response, request)
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()

	// we expect that our config was stored
	suite.Require().Equal(http.StatusOK, response.StatusCode)

	// cool get the cookie for next request
	for _, curCookie := range response.Cookies() {
		if curCookie.Name == "qsession" {
			cookie = curCookie
		}
	}
	return
}

func (suite *ACLTestSuite) HasAccess(username, path string, hasAccess bool) {

	// login to get cookie
	authCookie := suite.Login(username)
	suite.Require().NotNil(authCookie)

	// request response
	request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("http://%s/api/v1%s", suite.host, path), nil)
	request.AddCookie(authCookie)

	// action
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()

	// we expect that our config was stored
	if hasAccess {
		suite.Require().Equal(http.StatusOK, response.StatusCode)
	} else {
		suite.Require().NotEqual(http.StatusOK, response.StatusCode)
	}
}

func (suite *ACLTestSuite) Test04_ACL() {
	// we are have no user and try to have access
	suite.HasAccess("", "/data/all/get", false)

	// we try anon access to an allowed data
	suite.HasAccess("", "/anon/data/something", true)

	// we try an unknown user
	suite.HasAccess("unknown", "/data/set/stuff", false)

	suite.HasAccess("user", "/data/my/get", true)
	suite.HasAccess("admin", "/data/set/stuff", true)
}
