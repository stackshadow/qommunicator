package acl

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type ConfigStruct struct {
	Rules map[string][]string `yaml:"rules" json:"rules"`
}

func (c *ConfigStruct) MarshalZerologObject(e *zerolog.Event) {
	for username, acl := range c.Rules {
		e.Strs(username, acl)
	}
}

func (mdw *Middleware) configApplyDefault() {
	if mdw.config.Rules == nil {
		mdw.config.Rules = make(map[string][]string)
	}
}

func Set(newConfig ConfigStruct) {
	localMiddleware.Set(newConfig)
}

func (mdw *Middleware) Set(newConfig ConfigStruct) {
	// Lock
	mdw.configLock.Lock()
	defer mdw.configLock.Unlock()

	mdw.config = newConfig

	// apply defaults
	mdw.configApplyDefault()

	// log
	log.Debug().Object("config", &newConfig).Msg("set config")
}

func Get() (currentConfig ConfigStruct) {
	return localMiddleware.Get()
}

func (mdw *Middleware) Get() (currentConfig ConfigStruct) {
	return mdw.config
}

func Patch(newConfig ConfigStruct) {
	localMiddleware.Patch(newConfig)
}

func (mdw *Middleware) Patch(newConfig ConfigStruct) {

	// Lock
	mdw.configLock.Lock()
	defer mdw.configLock.Unlock()

	for username, rules := range newConfig.Rules {

		// user exist in config
		if existingACLs, exist := mdw.config.Rules[username]; exist {
			mdw.config.Rules[username] = append(existingACLs, rules...)
		} else { // user not exist
			mdw.config.Rules[username] = rules
		}

	}

	// log
	log.Debug().Object("config", &mdw.config).Msg("set config")
}
