package acl

// HasAccess check if the given user has access to this route
func (mdw Middleware) HasAccess(username string, route string) (hasAccess bool) {

	// We prevent config change in the middle
	mdw.configLock.Lock()
	defer mdw.configLock.Unlock()

	if rules, exist := mdw.config.Rules[username]; exist {

		for _, topic := range rules {

			if MatchRoute(route, topic) {
				hasAccess = true
				break
			}

		}

	}

	return
}
