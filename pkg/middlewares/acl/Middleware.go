package acl

import (
	"net/http"
	"strings"
	"sync"

	"github.com/rs/zerolog/log"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/session"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
)

type Middleware struct {
	config     ConfigStruct
	configLock *sync.Mutex
}

// storage
var localMiddleware Middleware

func init() {
	localMiddleware.Init()
}

func (mdw *Middleware) Init() {
	mdw.configLock = &sync.Mutex{}

	// set default
	mdw.configApplyDefault()
}

// HTTPMiddleware
func CheckMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {

		// respond with 500 if an error occurred
		defer utils.RespondOnPanic(w)

		// get username
		var username string
		currentSession := session.FromContext(r.Context())
		if currentSession != nil {
			username = currentSession.Username()
		}
		if username == "" {
			username = "_anonym"
		}

		route := strings.TrimRight(r.URL.Path, "/")

		hasAccess := localMiddleware.HasAccess(username, route)
		if !hasAccess {
			log.Error().Msgf("user '%s' has no access to resource '%s'", username, route)
			w.WriteHeader(http.StatusForbidden)
			return
		}

		// call next
		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}
