package acl

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gopkg.in/yaml.v3"
)

func (suite *ACLTestSuite) Test01_Config() {

	var err error

	configFileContent := `
---
middlewares:
  acl:
    rules:
      _anonym:
        - "/anon/data/#"
      admin:
        - "/data/#"
      user:
        - "/data/my/get"
`

	var configData ACTTestConfig

	// parse it
	err = yaml.Unmarshal([]byte(configFileContent), &configData)
	suite.Assert().Nil(err)

	// to json
	var jsonData []byte
	jsonData, err = json.Marshal(configData)
	suite.Assert().Nil(err)

	// request response
	request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("http://%s/api/v1/config", suite.host), nil)
	request.Method = "PUT"
	request.Body = ioutil.NopCloser(bytes.NewReader(jsonData))

	// action
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()

	// we expect 200
	suite.Require().Equal(http.StatusOK, response.StatusCode)

	// we check if values are correct parsed
	_, exist := localMiddleware.config.Rules["admin"]
	suite.Require().Equal(true, exist)

}

func (suite *ACLTestSuite) Test02_Patch() {

	var err error

	configFileContent := `
---
middlewares:
  acl:
    rules:
      additional:
        - "/anon/data/#"
      admin:
        - "/users/#"
`

	var configData ACTTestConfig

	// parse it
	err = yaml.Unmarshal([]byte(configFileContent), &configData)
	suite.Assert().Nil(err)

	// to json
	var jsonData []byte
	if err == nil {
		jsonData, err = json.Marshal(configData)
		suite.Assert().Nil(err)
	}

	// request response
	var request *http.Request
	if err == nil {
		request, err = http.NewRequest(http.MethodGet, fmt.Sprintf("http://%s/api/v1/config", suite.host), nil)
		request.Method = "PATCH"
		request.Body = ioutil.NopCloser(bytes.NewReader(jsonData))
	}

	// action
	var response *http.Response
	if err == nil {
		client := &http.Client{}
		response, err = client.Do(request)
		if err != nil {
			panic(err)
		}
		defer response.Body.Close()
	}

	// we expect 200
	suite.Require().Equal(http.StatusOK, response.StatusCode)

	// we check if values are correct parsed
	var exist bool
	_, exist = localMiddleware.config.Rules["admin"]
	suite.Require().Equal(true, exist)

	_, exist = localMiddleware.config.Rules["additional"]
	suite.Require().Equal(true, exist)

	amount := len(localMiddleware.config.Rules["admin"])
	suite.Require().Equal(2, amount)
}
