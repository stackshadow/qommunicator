package requestUUID

import (
	"context"
	"net/http"

	"github.com/google/uuid"
)

// ctxKeyRequestID
type ctxKeyRequestID int

// RequestIDKey is the key that holds the unique request ID in a request context.
const ctxRequestIDKey ctxKeyRequestID = 0

// Middleware that injects a request UUID into the context of each
// request.
func Middleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		requestID := uuid.Must(uuid.NewRandom()).String()
		ctx = context.WithValue(ctx, ctxRequestIDKey, requestID)
		next.ServeHTTP(w, r.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}

// GetReqID returns a request ID from the given context if one is present.
// Returns the empty string if a request ID cannot be found.
func GetReqID(ctx context.Context) (reqID string) {
	if ctx != nil {
		reqID, _ = ctx.Value(ctxRequestIDKey).(string)
	}
	return
}
