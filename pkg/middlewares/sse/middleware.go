package sse

import (
	"fmt"
	"net/http"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/message"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/session"
)

// Define our struct
type SSEMiddleware struct {
}

func (middleware *SSEMiddleware) Init() {

}

func (m SSEMiddleware) Name() string {
	return "sse"
}

// provide an server-send-event endpoint
func Endpoint(w http.ResponseWriter, r *http.Request) {

	var err error
	var flusher http.Flusher
	var flusherOk bool

	// get session from request, return if not exist
	currentSession := session.FromContext(r.Context())
	if currentSession == nil {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	// Make sure that the writer supports flushing.
	if err == nil {
		flusher, flusherOk = w.(http.Flusher)
		if !flusherOk {
			err = fmt.Errorf("streaming unsupported")
		}
	}

	// setup sse
	if err == nil {
		w.Header().Set("Content-Type", "text/event-stream")
		w.Header().Set("Cache-Control", "no-cache")
		w.Header().Set("Connection", "keep-alive")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.WriteHeader(http.StatusOK)

		log.Info().
			Object("session", currentSession).
			Msg("Welcome new client")

		// initial flush
		flusher.Flush()
	}

	if err == nil {
		// Old clients that disconnects
		contextDoneNotify := r.Context().Done()
		keepAliveTicker := time.NewTicker(45 * time.Second)

		// get the channel
		sendToFrontend := currentSession.ToFrontend()

		// inject the sse-version
		versionMessage := &message.Message{
			Topic:   "$METRIC/sse/version",
			Payload: "0.0.0",
		}
		currentSession.Send(versionMessage)

	worker:
		for {
			select {

			case <-contextDoneNotify:
				log.Info().
					Object("session", currentSession).
					Msg("contextDoneNotify occur")

				currentSession.CloseChannel()

				break worker

			case msg := <-sendToFrontend:
				if msg == nil {
					break worker
				}

				msgString, err := msg.JSONString()

				if err == nil {
					log.Debug().
						Object("session", currentSession).
						Str("topic", msg.Topic).
						Msg("Send message over HTTP2")

					// Write to the ResponseWriter
					// Server Sent Events compatible
					fmt.Fprintf(w, "data: %s\n\n", msgString)

					// Flush the data immediately instead of buffering it for later.
					flusher.Flush()

					keepAliveTicker.Reset(45 * time.Second)
				}

			// we need to send some data, to keep our connection
			// to the client-browser alive
			case <-keepAliveTicker.C:
				currentSession.Send(&message.Message{
					Topic: "sse/keepalive",
				})

			}

		}
	}

	if err == nil {
		log.Info().Object("session", currentSession).Msg("Disconnect occurred")
	}

}
