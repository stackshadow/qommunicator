package sessions

import (
	"net/http"

	"github.com/rs/zerolog/log"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
)

// provide a middleware that check if your session is valid
//
// This inject the session into your request-context and can be received with session.FromContext()
func (mdw *Middleware) CheckMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, req *http.Request) {

		// respond with 500 if an error occurred
		defer func() {
			if r := recover(); r != nil {
				if mdw.hookOnFail != nil {
					mdw.hookOnFail(req)
				}
				utils.RespondWithPanicInterface(r, w)
			}
		}()

		// try to extract the session from the jwt-token
		// of course, only if the JWT is known by us ;)
		currentSession := mdw.verify(req)
		if currentSession != nil {
			log.Debug().Object("session", currentSession).Msg("Session is valid")

			// inject session to context
			req = req.WithContext(currentSession.AddToContext(req.Context()))
		}

		if mdw.hookOnSuccess != nil {
			mdw.hookOnSuccess(req)
		}

		// call next
		if next != nil {
			next.ServeHTTP(w, req)
		}
	}

	return http.HandlerFunc(fn)
}

func CheckMiddleware(next http.Handler) http.Handler {
	return localMiddleware.CheckMiddleware(next)
}
