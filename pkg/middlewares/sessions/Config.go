package sessions

import (
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type ConfigStruct struct {
	Timeout   time.Duration `yaml:"timeout"`   // defaults to 60
	Intervall time.Duration `yaml:"intervall"` // defaults to 90
}

func (c *ConfigStruct) MarshalZerologObject(e *zerolog.Event) {
	e.Dur("timeout", c.Timeout)
	e.Dur("intervall", c.Intervall)
}

func (mdw *Middleware) configApplyDefault() {
	if mdw.config.Timeout == 0 {
		mdw.config.Timeout = 60 * time.Second
	}
	if mdw.config.Intervall == 0 {
		mdw.config.Intervall = 90 * time.Second
	}

	mdw.SetTimeout(mdw.config.Timeout)
	mdw.SetCheckIntervall(mdw.config.Intervall)
}

func Set(newConfig ConfigStruct) {
	localMiddleware.Set(newConfig)
}

func (mdw *Middleware) Set(newConfig ConfigStruct) {
	// Lock
	mdw.configLock.Lock()
	defer mdw.configLock.Unlock()

	mdw.config = newConfig

	// apply defaults
	mdw.configApplyDefault()

	// log
	log.Debug().Object("config", &newConfig).Msg("set config")
}
