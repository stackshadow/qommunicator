package sessions

import (
	"net/http"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/session"
)

// Sign a new jwt-token and store it to a cookie in w
func (middlware *Middleware) sign(curSession *session.Session, res http.ResponseWriter) (err error) {

	// create a signed JWT-Token
	var signedToken []byte
	signedToken, err = curSession.JWTNew()

	// set header
	if err == nil {
		res.Header().Set("Content-Type", "text/json")
		res.Header().Set("Cache-Control", "no-cache")
		res.Header().Set("Authorization", "Bearer "+string(signedToken))

		// set our jwt-token as cookie
		expire := time.Now().Add(time.Minute * 30)
		cookie := http.Cookie{
			Name:    "qsession",
			Value:   string(signedToken),
			Expires: expire,
		}
		http.SetCookie(res, &cookie)

		log.Debug().
			Object("session", curSession).
			Msg("Respond with new cookie")
	}

	return
}
