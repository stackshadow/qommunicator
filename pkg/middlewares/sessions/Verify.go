package sessions

import (
	"net/http"

	"github.com/lestrrat-go/jwx/jwt"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/qerrors"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/session"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
)

// Verify will check if your JWT-Token is known by us
//
// You only get an token when you are logged in
func (middlware *Middleware) verify(req *http.Request) (curSession *session.Session) {

	// get the cookie
	cookie, err := req.Cookie("qsession")
	utils.PanicOnErrWithStatus(err, http.StatusUnauthorized)

	// get the signed token
	jwtTokenSigned := []byte(cookie.Value)

	// we get the sessionID
	// ( this parse the token without verify the signature )
	var token jwt.Token
	token, err = jwt.Parse(jwtTokenSigned)
	utils.PanicOnErr(err)

	// get the sessionID
	var sessionID string
	if claimValueInterface, exist := token.Get(`sessionID`); exist {
		sessionID = claimValueInterface.(string)
	} else {
		utils.PanicOnErrWithStatus(qerrors.ErrCookieWrong, http.StatusUnauthorized)
		utils.PanicWithString("token not contains sessionID")
	}

	// get the session and verify the token
	var exist bool
	curSession, exist = middlware.sessions[sessionID]
	if !exist {
		utils.PanicOnErrWithStatus(qerrors.ErrSessionUnknown, http.StatusUnauthorized)
	}

	valid := curSession.JWTVerify(jwtTokenSigned)
	if !valid {
		utils.PanicOnErrWithStatus(qerrors.ErrSessionUnknown, http.StatusUnauthorized)
	}

	// update time
	curSession.Refresh()

	return
}
