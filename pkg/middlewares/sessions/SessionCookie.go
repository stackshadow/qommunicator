package sessions

import (
	"net/http"

	"gitlab.com/stackshadow/qommunicator/v2/pkg/session"
)

// SetNewSessionCookie create a new session and store it as cookie in r
func (mdw *Middleware) SetNewSessionCookie(res http.ResponseWriter, req *http.Request, username string) (newSession *session.Session) {
	var err error

	newSessionLocal := session.New(req.RemoteAddr)
	newSessionLocal.Reset(req.RemoteAddr)
	newSessionLocal.UsernameSet(username)
	newSessionLocal.AuthenticatedSet(true)

	// create a JWT and sign it
	err = mdw.sign(&newSessionLocal, res)
	if err == nil {
		mdw.register(&newSessionLocal)
		newSession = &newSessionLocal
	}

	return
}

// SetNewSessionCookie create a new session and store it as cookie in r
func SetNewSessionCookie(res http.ResponseWriter, req *http.Request, username string) (newSession *session.Session) {
	return localMiddleware.SetNewSessionCookie(res, req, username)
}
