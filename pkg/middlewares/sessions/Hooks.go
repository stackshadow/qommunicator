package sessions

import "net/http"

func (mdw *Middleware) HookOnFail(hook func(req *http.Request)) {
	mdw.hookOnFail = hook
}

func (mdw *Middleware) HookOnSuccess(hook func(req *http.Request)) {
	mdw.hookOnSuccess = hook
}
