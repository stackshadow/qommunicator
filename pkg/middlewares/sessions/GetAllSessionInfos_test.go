package sessions

import (
	"time"

	"gitlab.com/stackshadow/qommunicator/v2/pkg/session"
)

func (suite *TestSuite) TestGetSessionInfos() {

	localMiddleware.SetTimeout(time.Hour * 1)

	newSession1 := session.New("")
	newSession1.Reset("")
	newSession1.UsernameSet("frank")
	newSession1.AuthenticatedSet(true)
	localMiddleware.register(&newSession1)

	newSession2 := session.New("")
	newSession2.Reset("")
	newSession2.UsernameSet("frank")
	newSession2.AuthenticatedSet(true)
	localMiddleware.register(&newSession2)

	newSession3 := session.New("")
	newSession3.Reset("")
	newSession3.UsernameSet("merkur")
	newSession3.AuthenticatedSet(true)
	localMiddleware.register(&newSession3)

	// get session infos
	infos := localMiddleware.GetSessionInfos("frank")
	suite.Assert().Len(infos, 2)

	allInfos := localMiddleware.GetAllSessionInfos()
	suite.Assert().Len(allInfos, 3)

}
