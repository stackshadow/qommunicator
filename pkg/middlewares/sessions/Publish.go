package sessions

import "gitlab.com/stackshadow/qommunicator/v2/pkg/message"

func (middleware *Middleware) PublishToAll(msg *message.Message) {
	for _, session := range middleware.sessions {
		session.Send(msg)
	}
}
