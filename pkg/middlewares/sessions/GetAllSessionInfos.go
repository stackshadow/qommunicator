package sessions

import "gitlab.com/stackshadow/qommunicator/v2/pkg/session"

func (middleware *Middleware) GetAllSessionInfos() (infos []session.SessionInfo) {

	middleware.sessionLock.Lock()

	for _, session := range middleware.sessions {

		newInfo := session.SessionInfo()
		infos = append(infos, newInfo)

	}
	middleware.sessionLock.Unlock()

	return
}

// GetSessionInfos return all sessions for an given user
func (middleware *Middleware) GetSessionInfos(username string) (infos []session.SessionInfo) {

	middleware.sessionLock.Lock()
	for _, session := range middleware.sessions {

		if session.Username() == username {

			infos = append(infos, session.SessionInfo())
		}
	}
	middleware.sessionLock.Unlock()

	return
}
