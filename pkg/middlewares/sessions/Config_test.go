package sessions

import (
	"time"

	"gopkg.in/yaml.v3"
)

func (suite *TestSuite) Test01_Config() {

	var err error

	// load config
	configFileContent := `
---
timeout: 123s
intervall: 456s
`

	var configData ConfigStruct

	// parse it
	err = yaml.Unmarshal([]byte(configFileContent), &configData)
	suite.Assert().Nil(err)

	// save the config
	Set(configData)

	// we check if our values was stored
	suite.Require().Equal(time.Second*123, localMiddleware.config.Timeout)
	suite.Require().Equal(time.Second*456, localMiddleware.config.Intervall)
}
