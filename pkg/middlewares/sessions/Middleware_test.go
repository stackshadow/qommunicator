package sessions

import (
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/suite"
)

type TestSuite struct {
	suite.Suite
	cookie *http.Cookie
}

func TestSessionsMiddleware(t *testing.T) {
	suite.Run(t, new(TestSuite))
}

// setup suite
func (suite *TestSuite) SetupSuite() {
	// logger
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	log.Logger = log.Logger.With().Caller().Logger()
}

func (suite *TestSuite) Test02_Login() {

	res := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/login", nil)
	suite.Require().Nil(err)

	SetNewSessionCookie(res, req, "integrationtestuser")

	suite.cookie = nil
	var response http.Response
	response.Header = res.Header()
	for _, cookie := range response.Cookies() {
		if cookie.Name == "qsession" {
			suite.cookie = cookie
			break
		}
	}

	suite.Require().NotNil(suite.cookie)
}

func (suite *TestSuite) Test03_NoCookie() {

	res := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/login", nil)
	suite.Require().Nil(err)

	CheckMiddleware(nil).ServeHTTP(res, req)

	suite.Require().Equal(http.StatusUnauthorized, res.Code)
}

func (suite *TestSuite) Test04_ValidCookie() {

	res := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/login", nil)
	suite.Require().Nil(err)

	// add our auth-cookie to the request
	req.AddCookie(suite.cookie)

	CheckMiddleware(nil).ServeHTTP(res, req)

	suite.Require().Equal(http.StatusOK, res.Code)
}

func (suite *TestSuite) Test05_Timeout() {

	// set timeout
	localMiddleware.SetTimeout(500 * time.Millisecond)
	localMiddleware.SetCheckIntervall(500 * time.Millisecond)
	time.Sleep(2 * time.Second)

	res := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/login", nil)
	suite.Require().Nil(err)

	// add our auth-cookie to the request
	req.AddCookie(suite.cookie)

	CheckMiddleware(nil).ServeHTTP(res, req)

	// we expect that our config was stored
	suite.Require().Equal(http.StatusUnauthorized, res.Code)

}
