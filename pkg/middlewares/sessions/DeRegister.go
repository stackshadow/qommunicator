package sessions

import "github.com/rs/zerolog/log"

// DeRegister close and remove an session
//
// This function do not LOCK the sessionlist
func (mdw *Middleware) DeRegister(sessionID string) {
	oldSession := mdw.sessions[sessionID]

	oldSession.Close()
	delete(mdw.sessions, sessionID)
	log.Info().Object("session", oldSession).Msg("DeRegister")
}
