package sessions

import (
	"net/http"
	"sync"

	"gitlab.com/stackshadow/qommunicator/v2/pkg/session"
)

// Define our struct
type Middleware struct {
	config             ConfigStruct
	configLock         *sync.Mutex
	sessions           map[string]*session.Session
	sessionLock        *sync.Mutex
	checkTickerRefresh chan bool

	hookOnFail    func(req *http.Request) // this function will be called when an error occurred
	hookOnSuccess func(req *http.Request)
}

// we store an local instance to use package-functions
var localMiddleware Middleware

// we init the localMiddleware
func init() {
	localMiddleware.Init()
}

func (mdw *Middleware) Init() {
	mdw.configLock = &sync.Mutex{}
	mdw.sessions = make(map[string]*session.Session)
	mdw.sessionLock = &sync.Mutex{}
	mdw.checkTickerRefresh = make(chan bool, 5)

	// set default
	mdw.configApplyDefault()

	// run cleanup in background
	go mdw.cleanupJob()
}

func (mdw Middleware) Name() string {
	return "sessions"
}
