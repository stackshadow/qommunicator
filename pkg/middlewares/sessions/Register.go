package sessions

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/session"
)

// Register register a already created session.
//
// This function LOCK the sessionlist
func (middleware *Middleware) register(newSession *session.Session) {
	middleware.sessionLock.Lock()

	middleware.sessions[newSession.ID()] = newSession
	log.Info().Object("session", newSession).Msg("Register")

	middleware.sessionLock.Unlock()
}
