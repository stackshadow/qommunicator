package sessions

import (
	"time"

	"github.com/rs/zerolog/log"
)

// SetTimeout will set the timeout for an existing session
// the session will be removed after timeout when no action is performed by the user
func (mdw *Middleware) SetTimeout(timeout time.Duration) {
	mdw.config.Timeout = timeout
	log.Info().Dur("timeout", timeout).Msg("Set of timeout for session")
	mdw.checkTickerRefresh <- true
}

// SetCheckInterval set the intervall for check of sessions
func (mdw *Middleware) SetCheckIntervall(intervall time.Duration) {
	mdw.config.Intervall = intervall
	log.Info().Dur("intervall", intervall).Msg("Set intervall for session-check")
	mdw.checkTickerRefresh <- true
}

// cleanupJob will check for every intervall if the session is older than sessionTimeout
func (mdw *Middleware) cleanupJob() {
	log.Debug().Msg("Start cleanupJob")
	checkTicker := time.NewTicker(mdw.config.Intervall)
	for {
		select {

		case <-checkTicker.C:

			log.Debug().
				Dur("timeout", mdw.config.Timeout).
				Dur("interval", mdw.config.Intervall).
				Msg("Check all sessions for timeout")

			mdw.sessionLock.Lock()
			for sessionID, session := range mdw.sessions {

				if session.IsTimedOut(mdw.config.Timeout) {
					log.Info().Object("session", session).Msg("Session timed out, we remove the session.")

					// close channel
					mdw.DeRegister(sessionID)
				}

			}
			mdw.sessionLock.Unlock()
			checkTicker.Reset(mdw.config.Intervall)

		case <-mdw.checkTickerRefresh:
			log.Debug().Msg("Time changed, reset checkTimer")
			checkTicker.Reset(mdw.config.Intervall)
		}

	}
}
