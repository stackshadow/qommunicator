package localuser

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
	"golang.org/x/crypto/bcrypt"
)

type ConfigStruct struct {
	Cost  int64             `yaml:"cost" json:"cost,omitempty"` // bcrypt-cost
	Users map[string]string `yaml:"users" json:"users,omitempty"`
}

func (c *ConfigStruct) MarshalZerologObject(e *zerolog.Event) {
	e.Int64("cost", c.Cost)
	for username, _ := range c.Users {
		e.Str("user", username)
	}
}

func (mdw *Middleware) configApplyDefault() {

	if mdw.config.Cost == 0 {
		mdw.config.Cost = 15
	}

}

func Set(newConfig ConfigStruct) {
	localMiddleware.Set(newConfig)
}

func (mdw *Middleware) Set(newConfig ConfigStruct) {

	// Lock
	mdw.configLock.Lock()
	defer mdw.configLock.Unlock()

	mdw.config = newConfig

	// apply defaults
	mdw.configApplyDefault()

	// log
	log.Debug().Object("config", &newConfig).Msg("set config")

	return
}

func Patch(newConfig ConfigStruct) {
	localMiddleware.Patch(newConfig)
}

func (mdw *Middleware) Patch(newConfig ConfigStruct) {

	// Lock
	mdw.configLock.Lock()
	defer mdw.configLock.Unlock()

	// patch cost
	if newConfig.Cost != 0 {
		mdw.config.Cost = newConfig.Cost
	}

	if mdw.config.Users == nil {
		mdw.config.Users = make(map[string]string)
	}

	// iterate over new users
	for newUsername, newCleartextPassword := range newConfig.Users {

		// create new password and store it
		newPassword, err := bcrypt.GenerateFromPassword([]byte(newCleartextPassword), int(mdw.config.Cost))
		utils.PanicOnErr(err)

		// patch the password
		mdw.config.Users[newUsername] = string(newPassword)
	}

	return
}
