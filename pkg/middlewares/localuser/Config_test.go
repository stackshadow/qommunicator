package localuser

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gopkg.in/yaml.v2"
)

func (suite *TestSuite) Test01_PatchConfig() {

	var err error

	configFileContent := `
---
cost: 5
users:
  admin: adminpassword
  user: userpassword
`

	var configData ConfigStruct

	// parse it
	err = yaml.Unmarshal([]byte(configFileContent), &configData)
	suite.Assert().Nil(err)

	// to json
	var jsonData []byte
	if err == nil {
		jsonData, err = json.Marshal(configData)
	}
	suite.Assert().Nil(err)

	// request response
	var request *http.Request
	if err == nil {
		request, err = http.NewRequest(http.MethodGet, fmt.Sprintf("http://%s/api/v1/config", suite.host), nil)
		request.Method = "PATCH"
		request.Body = ioutil.NopCloser(bytes.NewReader([]byte(jsonData)))
	}

	// action
	var response *http.Response
	if err == nil {
		client := &http.Client{}
		response, err = client.Do(request)
		if err != nil {
			panic(err)
		}
		defer response.Body.Close()
	}

	// we expect 200
	suite.Require().Equal(http.StatusOK, response.StatusCode)

	// check password of non existing user
	err = CheckPassword("nonexisting", "somepassword")
	suite.Assert().NotNil(err)

	// check if password is correct
	err = CheckPassword("admin", "adminpassword")
	suite.Assert().Nil(err)

	// check if password is correct
	err = CheckPassword("user", "userpassword")
	suite.Assert().Nil(err)
}

func (suite *TestSuite) Test02_SetConfig() {

	var err error

	// copy to local config
	localConfig := localMiddleware.config
	delete(localConfig.Users, "admin")

	Set(localConfig)

	if err == nil {
		// check password of non existing user
		err = CheckPassword("nonexisting", "somepassword")
		suite.Assert().NotNil(err)

		// check if password is correct
		err = CheckPassword("admin", "adminpassword")
		suite.Assert().NotNil(err)

		// check if password is correct
		err = CheckPassword("user", "userpassword")
		suite.Assert().Nil(err)
	}

}
