package localuser

import (
	"errors"
	"sync"

	"golang.org/x/crypto/bcrypt"
)

type Middleware struct {
	config     ConfigStruct
	configLock *sync.Mutex
}

// storage
var localMiddleware Middleware

func init() {
	localMiddleware.Init()
}

func (mdw *Middleware) Init() {
	mdw.configLock = &sync.Mutex{}

	// set default
	mdw.configApplyDefault()
}

// return error of user not exist or password is wrong
//
// to avoid spoofing, we not differe between not existing user and a wrong password
func (mdw *Middleware) CheckPassword(username, password string) (err error) {

	// get the hashed password
	hashedPassword, exist := mdw.config.Users[username]
	if !exist {
		err = errors.New("password wrong")
		return
	}

	// check hash
	err = bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	if err != nil {
		err = errors.New("password wrong")
		return
	}

	return
}

// HTTPMiddleware
func CheckPassword(username, password string) (err error) {
	return localMiddleware.CheckPassword(username, password)
}
