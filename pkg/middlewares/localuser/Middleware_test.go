package localuser

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"testing"

	"github.com/go-chi/chi/v5"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/suite"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/middlewares/stripprefix"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
)

type TestSuite struct {
	suite.Suite
	host   string
	router *chi.Mux
}

type TestCredentials struct {
	Username string `yaml:"username" json:"username,omitempty"`
	Password string `yaml:"password" json:"password,omitempty"`
}

func TestDynDBTestSuite(t *testing.T) {
	suite.Run(t, new(TestSuite))
}

// setup suite
func (suite *TestSuite) SetupSuite() {
	// logger
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	log.Logger = log.Logger.With().Caller().Logger()

	suite.host = "127.0.0.1:4502"

	// new router
	suite.router = chi.NewRouter()

	// this endpoint is just for our testframework to wait until the server is up and running
	suite.router.Get("/up", func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(http.StatusOK) })

	// we serve our example under /api/v1
	suite.router.Route("/api/v1", func(r chi.Router) {

		// remove /api/v1 from request
		r.Use(stripprefix.Middleware("/api/v1"))

		// endpoint to set the config
		r.Put("/config", func(res http.ResponseWriter, req *http.Request) {
			var err error
			defer utils.RespondOnPanic(res)

			// read from request
			var bodyData []byte
			bodyData, err = io.ReadAll(req.Body)
			utils.PanicOnErr(err)

			// parse
			var newConfig ConfigStruct
			err = json.Unmarshal(bodyData, &newConfig)
			utils.PanicOnErr(err)

			// this will overwrite the whole config
			Set(newConfig)
		})

		// endpoint to patch ( add a new user ) the config
		r.Patch("/config", func(res http.ResponseWriter, req *http.Request) {

			var err error
			defer utils.RespondOnPanic(res)

			// read from request
			var bodyData []byte
			bodyData, err = io.ReadAll(req.Body)
			utils.PanicOnErr(err)

			// parse
			var newConfig ConfigStruct
			err = json.Unmarshal(bodyData, &newConfig)
			utils.PanicOnErr(err)

			// this will update the config with the content
			Patch(newConfig)
		})

		// we try to login
		r.Post("/login", func(res http.ResponseWriter, req *http.Request) {

			var err error
			defer func() {
				if err == nil {
					res.WriteHeader(http.StatusOK)
				} else {
					res.Write([]byte(err.Error()))
					res.WriteHeader(http.StatusUnauthorized)
				}
			}()

			// read all data from request
			var bodyData []byte
			bodyData, err = io.ReadAll(req.Body)

			// parse it
			var creds TestCredentials
			if err == nil {
				err = json.Unmarshal(bodyData, &creds)
			}

			if err == nil {
				err = CheckPassword(creds.Username, creds.Password)
			}

		})
	})

	// start the server
	go http.ListenAndServe(suite.host, suite.router)

	// wait until the server is ready
	utils.WaitForServer(fmt.Sprintf("http://%s/up", suite.host))

}
