package failban

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/suite"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
)

type TestSuite struct {
	suite.Suite
	host       string
	middleware *Middleware
	router     *chi.Mux
}

func TestSuiteTests(t *testing.T) {
	suite.Run(t, new(TestSuite))
}

func (suite *TestSuite) SetupSuite() {
	// logger
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	log.Logger = log.Logger.With().Caller().Logger()

	suite.host = "127.0.0.1:3939"

	// tag::create[]
	// we create a new middleware instance
	var fail2banMiddleware Middleware

	fail2banMiddleware.Init() // we init this middleware, this is NEEDED and start the background-check-job
	fail2banMiddleware.TimerSet(
		time.Second*1, // we set the time on how often the check-routine will check all strikes
		time.Second*5, // this is the timeout after the maximum of strikes are reached
	)
	fail2banMiddleware.StrikesMax = 5 // a user can fail 5 times
	// end::create[]

	// tag::router[]
	// we create a new router
	suite.router = chi.NewRouter()

	// we create a group that represent all our "protected" endpoints
	suite.router.Group(func(r chi.Router) {

		// here the magic happens
		// this middleware check, if the source-ip of the request is blocked
		r.Use(fail2banMiddleware.CheckMiddleware)

		// and this is an example endpoint
		r.Post("/secret", func(res http.ResponseWriter, req *http.Request) {

			// check if we get the correct token
			authHeader := req.Header.Get("Authorization")

			// we we not get the correct token, we strike
			if authHeader != "Bearer myFancyToken" {

				// increment the strike counter
				fail2banMiddleware.Strike(req)

				// and answer that you are not authorized
				res.WriteHeader(http.StatusUnauthorized)
				return
			}

			// if everything ok, we return ok
			res.WriteHeader(http.StatusOK)
			res.Write([]byte("ok"))
		})
	})
	// end::router[]

	// this endpoint is just for our testing
	suite.router.Get("/up", func(res http.ResponseWriter, req *http.Request) {
		res.WriteHeader(http.StatusOK)
	})

	suite.middleware = &fail2banMiddleware

	go http.ListenAndServe(":3939", suite.router)
	utils.WaitForServer("http://" + suite.host + "/up")
}

func (suite *TestSuite) Test01_Banit() {

	// we form a new request
	request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("http://%s/secret", suite.host), nil)
	request.Method = "POST"

	requestGood, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("http://%s/secret", suite.host), nil)
	requestGood.Method = "POST"
	requestGood.Header.Set("Authorization", "Bearer myFancyToken")

	// we strike under the limit
	for j := 1; j <= 3; j++ {
		client := &http.Client{}
		response, err := client.Do(request)
		utils.PanicOnErr(err)

		suite.Assertions.Equal(http.StatusUnauthorized, response.StatusCode)

		response.Body.Close()
	}

	client := &http.Client{}
	response, err := client.Do(requestGood)
	utils.PanicOnErr(err)

	// we should get "ok"
	responseData, err := ioutil.ReadAll(response.Body)
	utils.PanicOnErr(err)

	suite.Assertions.Equal([]byte("ok"), responseData)

	response.Body.Close()

	// strike more
	for j := 1; j <= 2; j++ {
		client := &http.Client{}
		response, err := client.Do(request)
		utils.PanicOnErr(err)

		suite.Assertions.Equal(http.StatusUnauthorized, response.StatusCode)

		response.Body.Close()
	}

	// now we should get forbidden
	client = &http.Client{}
	response, err = client.Do(requestGood)
	utils.PanicOnErr(err)
	suite.Assertions.Equal(http.StatusForbidden, response.StatusCode)
	response.Body.Close()

	// wait 2 seconds
	time.Sleep(2 * time.Second)

	// should be banned
	client = &http.Client{}
	response, err = client.Do(requestGood)
	utils.PanicOnErr(err)
	suite.Assertions.Equal(http.StatusForbidden, response.StatusCode)
	response.Body.Close()

	// wait longer
	time.Sleep(7 * time.Second)

	// should not be banned anymore
	client = &http.Client{}
	response, err = client.Do(requestGood)
	utils.PanicOnErr(err)
	suite.Assertions.Equal(http.StatusOK, response.StatusCode)
	response.Body.Close()

}
