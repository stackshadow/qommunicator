package failban

import (
	"time"

	"github.com/mcuadros/go-defaults"
	"github.com/rs/zerolog/log"
)

func (mdw *Middleware) Init() {

	// create the map
	if mdw.strikes == nil {
		mdw.strikes = make(map[string]*strike)
	}
	if mdw.timerupdate == nil {
		mdw.timerupdate = make(chan bool, 5)
	}

	// apply defaults
	defaults.SetDefaults(mdw)

	log.Debug().Msg("Start cleanupJob")
	checkTicker := time.NewTicker(mdw.Checktick)

	go func() {
		for {
			select {

			case <-checkTicker.C:

				log.Debug().Msg("Check strikes for timeout")
				mdw.mutex.Lock()

				for strikeAddr, strike := range mdw.strikes {

					if time.Now().After(strike.laststrike.Add(mdw.Timeout)) {
						strike.laststrike = time.Now()
						strike.strikes--
						log.Debug().
							Str("ip", strikeAddr).
							Uint32("strikes", strike.strikes).Msg("reduce")
					}
					if strike.strikes == 0 {
						delete(mdw.strikes, strikeAddr)
					}
				}

				mdw.mutex.Unlock()
				checkTicker.Reset(mdw.Checktick)

			case <-mdw.timerupdate:
				log.Debug().Msg("time changed, reset checkTimer")
				checkTicker.Reset(mdw.Checktick)
			}

		}
	}()

}
