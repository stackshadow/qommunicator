package failban

import (
	"net/http"
	"time"

	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
)

// provide a middleware that check if the source IP is banned
func (mdw *Middleware) CheckMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {

		// respond with 500 if an error occurred
		defer utils.RespondOnPanic(w)

		// check if IP is banned and call next if not
		if !mdw.IsBanned(r) {
			next.ServeHTTP(w, r)
		} else {
			time.Sleep(mdw.HoldTime)
			w.WriteHeader(http.StatusForbidden)
		}
	}

	return http.HandlerFunc(fn)
}

// provide a middleware that check if the source IP is banned
func CheckMiddleware(next http.Handler) http.Handler {
	return localMiddleware.CheckMiddleware(next)
}
