package failban

import (
	"net"
	"net/http"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
)

type strike struct {
	laststrike time.Time
	strikes    uint32
}

func (mdw *Middleware) Strike(req *http.Request) {

	mdw.mutex.Lock()
	defer mdw.mutex.Unlock()

	remoteAddr, _, err := net.SplitHostPort(req.RemoteAddr)
	utils.PanicOnErr(err)

	currentStrike, exist := mdw.strikes[remoteAddr]

	// not exist, this is the first strike
	if !exist {
		currentStrike = &strike{}
		mdw.strikes[remoteAddr] = currentStrike
	}

	// exist add a strike
	currentStrike.laststrike = time.Now()
	currentStrike.strikes++
	log.Warn().
		Str("ip", remoteAddr).
		Uint32("strikes", currentStrike.strikes).
		Msg("strike")

}
