package failban

import (
	"net"
	"net/http"

	"github.com/rs/zerolog/log"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
)

func (mdw *Middleware) IsBanned(req *http.Request) (banned bool) {

	remoteAddr, _, err := net.SplitHostPort(req.RemoteAddr)
	utils.PanicOnErr(err)

	currentStrike, exist := mdw.strikes[remoteAddr]
	if exist {
		if currentStrike.strikes >= mdw.StrikesMax {
			log.Warn().
				Str("ip", remoteAddr).
				Msg("is banned")
			banned = true
		}
	}

	return
}
