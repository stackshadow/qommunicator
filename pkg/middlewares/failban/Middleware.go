package failban

import (
	"sync"
	"time"
)

// Define our struct
type Middleware struct {
	mutex      sync.Mutex
	strikes    map[string]*strike
	StrikesMax uint32        `default:"3"`
	Checktick  time.Duration `default:"1m"`
	Timeout    time.Duration `default:"3m"`
	HoldTime   time.Duration `default:"500ms"` // this is the time, we hold a client in line when it is banned

	timerupdate chan bool // this channel fire when we update the timer
}

// we store an local instance to use package-functions
var localMiddleware Middleware

// we init the localMiddleware
func Init() {
	localMiddleware.Init()
}
