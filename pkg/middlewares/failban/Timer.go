package failban

import "time"

func (mdw *Middleware) TimerSet(tick time.Duration, timeout time.Duration) {

	mdw.mutex.Lock()
	defer mdw.mutex.Unlock()

	mdw.Checktick = tick
	mdw.Timeout = timeout
	mdw.timerupdate <- true
}
