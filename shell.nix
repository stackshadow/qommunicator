{ pkgs ? import <nixpkgs> { } }:
let
  lib = pkgs.lib;
  inherit (lib) sourceByRegex;

  nixosGoCyclo = import
    (pkgs.fetchzip {
      url = "https://github.com/NixOS/nixpkgs/archive/bf01537f0c9deccf7906b51e101d05c039390bb8.zip";
      sha256 = "sha256-fgPiS1heTNSi5i+22pMxoj7t/iOg42zRZJqxeTCJPjU=";
    })
    { };
  gocyclo = nixosGoCyclo.gocyclo;

  gobadgePackage = import (builtins.fetchTarball https://gitlab.com/stackshadow/gobadge-cli/-/archive/main/gobadge-cli-main.tar.gz) { };

  gobadge = import
    (builtins.fetchTarball {
      url = "https://gitlab.com/stackshadow/gobadge-cli/-/archive/v1.3.0/gobadge-cli-v1.3.0.tar.gz";
      sha256 = "sha256:0sml2rph7c9jmwn7lfr74hinmwkf8j2mbcjc6ciyx5i3cy38z1cv";
    })
    { };

  gobadgeSource =
    builtins.fetchGit
      {
        name = "dyndb";
        url = "https://gitlab.com/stackshadow/gobadge-cli.git";
        ref = "refs/heads/main";
        rev = "4d6fd041fd5ff87b7c62d5bcccf9075124c86df8";
      };
in
pkgs.mkShell {
  buildInputs = with pkgs; [ go gnumake gocyclo gosec gobadge.package ];
  shellHook = ''
    clean() {
      make -f ${gobadgeSource}/build/local/badges.mk clean
    }
    coverage() {
      make -f ${gobadgeSource}/build/local/badge-coverage.mak badge
    }
    cyclo() {
      make -f ${gobadgeSource}/build/local/badge-gocyclo.mak badge
    }
    sec() {
      make -f ${gobadgeSource}/build/local/badge-gosec.mak badge
    }
    lastbuild(){
      make -f ${gobadgeSource}/build/local/badge-lastbuild.mak badge
    }
  '';
}

