module gitlab.com/stackshadow/qommunicator/v2

go 1.16

// for local develop
replace gitlab.com/gopilot/lib => ../gopilot/lib

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-chi/chi/v5 v5.0.4
	github.com/google/uuid v1.2.0
	github.com/lestrrat-go/jwx v1.1.7
	github.com/mcuadros/go-defaults v1.2.0
	github.com/rs/zerolog v1.21.0
	github.com/sethvargo/go-retry v0.1.0
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20211117183948-ae814b36b871
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)
