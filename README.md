# Qommunicator

Middleware framework that provides

- acl
  - access-control of endpoints
- sessions
  - recognize your users
- sse
  - sent message to clients with server-sent-events, this needs sessions !

## Why ?

To simplify common tasks like session-management and async-communication with web-apps

## Documentation

Check the docs-folder