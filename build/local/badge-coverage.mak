.ONESHELL:
SHELL=bash

CLEANTARGETS=
ARCH ?= amd64

clean: 
	make -f build/local/badge-coverage.mak $(CLEANTARGETS)

badge: coverage.svg

coverage.out:
	go test -v -timeout 60s -parallel 1 -coverprofile=coverage.out  $$(go list ./... | grep -v /mocked)
	go tool cover -func coverage.out
coverage.out.clean:
	@rm -vf $$(basename -s .clean $@)
CLEANTARGETS+=coverage.out.clean

coverage.env: coverage.out
	@value=$$(go tool cover -func coverage.out | grep total: | awk '{print $$3}' | sed 's/%//g')
	@echo "export LABEL=coverage" > $@
	@echo "export FILENAME=./coverage.svg" >> $@
	@echo "export VALUE_MIN=0.0" >> $@
	@echo "export VALUE_MAX=100.0" >> $@
	@echo "export VALUE=$$value" >> $@
coverage.env.clean:
	@rm -vf $$(basename -s .clean $@)
CLEANTARGETS+=coverage.env.clean

coverage.svg: coverage.env
	source $<
	echo "$$FILENAME"
	gobadge
coverage.svg.clean:
	@rm -vf $$(basename -s .clean $@)
CLEANTARGETS+=coverage.svg.clean
