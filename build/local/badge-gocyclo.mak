.ONESHELL:
SHELL=bash

CLEANTARGETS=
ARCH ?= amd64

clean: 
	make -f build/local/badge-gocyclo.mak $(CLEANTARGETS)

badge: gocyclo.svg

gocyclo.out:
	gocyclo -avg -ignore vendor . > $@
gocyclo.out.clean:
	@rm -vf $$(basename -s .clean $@)
CLEANTARGETS+=gocyclo.out.clean

gocyclo.env: gocyclo.out
	@value=$$(cat $< | grep Average | cut -d':' -f2 | sed -e 's/\s*//')
	@echo "export LABEL=gocyclo" > $@
	@echo "export FILENAME=./gocyclo.svg" >> $@
	@echo "export VALUE_MIN=10.0" >> $@
	@echo "export VALUE_MAX=0.01" >> $@
	@echo "export VALUE=$$value" >> $@
gocyclo.env.clean:
	@rm -vf $$(basename -s .clean $@)
CLEANTARGETS+=gocyclo.env.clean

gocyclo.svg: gocyclo.env
	source $<
	echo "$$FILENAME"
	gobadge
gocyclo.svg.clean:
	@rm -vf $$(basename -s .clean $@)
CLEANTARGETS+=gocyclo.svg.clean
