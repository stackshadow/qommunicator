.ONESHELL:
SHELL=bash

CLEANTARGETS=
ARCH ?= amd64

clean: 
	make -f build/local/badge-gosec.mak $(CLEANTARGETS)

badge: gosec.svg

gosec.out:
	gosec -color=false -no-fail -severity medium ./... > $@
gosec.out.clean:
	@rm -vf $$(basename -s .clean $@)
CLEANTARGETS+=gosec.out.clean

gosec.env: gosec.out
	@value=$$(cat $< | grep Issues | cut -d':' -f2 | sed -e 's/\s*//')
	@echo "export LABEL=gosec" > $@
	@echo "export FILENAME=./gosec.svg" >> $@
	@echo "export VALUE_MIN=0.0" >> $@
	@echo "export VALUE_MAX=100.0" >> $@
	@echo "export VALUE=$$value" >> $@
gosec.env.clean:
	@rm -vf $$(basename -s .clean $@)
CLEANTARGETS+=gosec.env.clean

gosec.svg: gosec.env
	source $<
	echo "$$FILENAME"
	gobadge
gosec.svg.clean:
	@rm -vf $$(basename -s .clean $@)
CLEANTARGETS+=gosec.svg.clean
