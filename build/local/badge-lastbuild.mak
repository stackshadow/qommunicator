.ONESHELL:
SHELL=bash

CLEANTARGETS=
ARCH ?= amd64

clean: 
	make -f build/local/badge-lastbuild.mak $(CLEANTARGETS)

badge: lastbuild.svg

lastbuild.env:
	@value=$$(date +%d.%m.%Y)
	@echo "export LABEL=lastbuild" > $@
	@echo "export FILENAME=./lastbuild.svg" >> $@
	@echo "export VALUE_MIN=0.0" >> $@
	@echo "export VALUE_MAX=100.0" >> $@
	@echo "export VALUE=$$value" >> $@
lastbuild.env.clean:
	@rm -vf $$(basename -s .clean $@)
CLEANTARGETS+=lastbuild.env.clean

lastbuild.svg: lastbuild.env
	source $<
	echo "$$FILENAME"
	gobadge --text
lastbuild.svg.clean:
	@rm -vf $$(basename -s .clean $@)
CLEANTARGETS+=lastbuild.svg.clean

